using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlickerController : MonoBehaviour
{
    public bool isFlickering = false;

    [SerializeField] 
    private Light[] lights;

    [SerializeField] 
    private float flickerSpeed = 10f;
    [SerializeField] 
    private float minIntensity = 0.5f;

    private float[] baseIntensity;
    private float   flickerIntensity;
    private float   flickerTimer = 0f;

    private void Awake()
    {
        baseIntensity = new float[lights.Length];
        for (int i = 0; i < lights.Length; i++)
            baseIntensity[i] = lights[i].intensity;
    }

    private void Update()
    {
        if (isFlickering == false)
        {
            for (int i = 0; i < lights.Length; i++)
                lights[i].intensity = baseIntensity[i];
            return;
        }

        // Update flicker timer
        flickerTimer += Time.deltaTime * flickerSpeed;

        // Set light intensity to the sum of the base intensity and the flicker intensity
        for(int i = 0; i < lights.Length; i++)
        {
            // Calculate flicker intensity using Perlin noise
            flickerIntensity = Mathf.Lerp(minIntensity, baseIntensity[i], Mathf.PerlinNoise(flickerTimer, 0));

            Light light = lights[i];
            light.intensity = baseIntensity[i] + flickerIntensity;
        }
    }
}