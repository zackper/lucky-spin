using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeController : MonoBehaviour
{
    [SerializeField]
    private new Camera camera;

    public float targetIntensity    = 0f;
    public float smoothTime         = 1.5f;

    // Ramp Intensity value
    private float intensity         = 0f;
    private float smoothVelocity    = 0f;

    [SerializeField] 
    private float shakeDuration     = 0.5f;
    [SerializeField] 
    private float shakePosition     = 0.1f;
    [SerializeField]
    private float shakeRotation     = 0.1f;

    private Vector3     originalPosition;
    private Quaternion  originalRotation;


    private void Awake()
    {
        originalPosition = camera.transform.localPosition;
        originalRotation = camera.transform.localRotation;
    }

    private void Update()
    {
        if (intensity > 0)
        {
            // Generate random offsets for position and rotation
            float offsetX       = Random.Range(-shakePosition, shakePosition) * intensity;
            float offsetY       = Random.Range(-shakePosition, shakePosition) * intensity;
            float offsetZ       = Random.Range(-shakePosition, shakePosition) * intensity;
            float offsetAngle   = Random.Range(-shakeRotation, shakeRotation) * intensity;

            // Apply offsets to camera's local position and rotation
            camera.transform.localPosition = originalPosition + new Vector3(offsetX, offsetY, offsetZ);
            camera.transform.localRotation = originalRotation * Quaternion.Euler(0f, 0f, offsetAngle);
        }
        else
        {
            // Reset camera position and rotation
            camera.transform.localPosition = originalPosition;
            camera.transform.localRotation = originalRotation;
        }

        intensity = Mathf.SmoothDamp(intensity, targetIntensity, ref smoothVelocity, smoothTime);
    }
}