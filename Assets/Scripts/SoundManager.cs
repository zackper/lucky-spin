using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundEffectType
{
    AMBIENT,
    EARTHQUAKE,
    FLICK_SWITCH,
    OBJECT_DRAG,

    HM,
    RISE,
    WELCOME,
    WILL_YOU_PAY,

    MINOR_PAIN,

    DARK_MAGIC
}

public class SoundManager : MonoBehaviour
{
    public AudioSource                              source;
    public Dictionary<SoundEffectType, SoundEffect> dispatcher = new Dictionary<SoundEffectType, SoundEffect>();

    [SerializeField]                                // SOs for initializing dispatcher
    private List<SoundEffect>                       soundEffectList = new List<SoundEffect>();

    public List<SoundEffect>                        debug_soundEffects
    {
        get
        {
            return soundEffectList;
        }
    }

    public void         PlaySound(SoundEffectType type, float volume = 1f, float delay = 0f)
    {
        SoundEffect effect = dispatcher[type];

        if (delay > 0f)
            StartCoroutine(PlaySoundDelayed(effect.type, effect.defaultVolume * volume, effect.defaultDelay + delay));
        else
            source.PlayOneShot(effect.clip, effect.defaultVolume * volume);
    }
    private IEnumerator PlaySoundDelayed(SoundEffectType type, float volume, float delay)
    {
        yield return new WaitForSeconds(delay);

        SoundEffect effect = dispatcher[type];
        source.PlayOneShot(effect.clip, effect.defaultVolume);
    }


    private void Initialize()
    {
        Debug.Assert(source != null, "Add reference to source");

        foreach(SoundEffect soundEffect in soundEffectList)
        {
            if (dispatcher.ContainsKey(soundEffect.type))
            {
                Debug.Assert(false, "Each sound effect type should only exist once");
                continue;
            }
            dispatcher.Add(soundEffect.type, soundEffect);
        }
    }

    #region Singleton
    public static SoundManager Instance { get; private set; }
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            Initialize();
        }
    }
    #endregion
}
