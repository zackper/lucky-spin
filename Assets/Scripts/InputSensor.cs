using System;
using UnityEngine.Events;
using UniRx;
using UnityEngine;

public class InputSensor : MonoBehaviour
{
    public readonly UnityEvent OnUserClick = new UnityEvent();

    private void Awake()
    {
        var clickStream = Observable.EveryUpdate()
               .Where(_ => Input.GetMouseButtonDown(0));

        clickStream.Buffer(clickStream.Throttle(TimeSpan.FromMilliseconds(250)))
                .Where(xs => xs.Count >= 1)
                .Subscribe(xs =>
                {
                    OnUserClick.Invoke();
                });
    }
}
