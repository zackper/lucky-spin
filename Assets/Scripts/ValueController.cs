using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UniRx;
using UnityEngine.Networking;
using System.Net;

public class ValueController : MonoBehaviour
{
    private List<float> values;

    public TMP_Text     cull1Value;
    public TMP_Text     cull2Value;

    public bool         flipCoin = true;

    [Header("Game Events")]
    [SerializeField]
    private GameEvent onHalfRotation;

    private void Awake()
    {
        values = new List<float>
        {
            1000,
            2000,
            5000,
            10000,
            15000,
            30000,
            100000,
            150000,
            200000
        };

        cull1Value.text = GetRandomValue().ToString();
        cull2Value.text = GetRandomValue().ToString();

        onHalfRotation.RegisterListener(SubscribeOnHalfRotation);
    }
    private void OnDestroy()
    {
        onHalfRotation.UnregisterListener(SubscribeOnHalfRotation);
    }
    public void Reset()
    {
        flipCoin = true;
    }

    private void SubscribeOnHalfRotation()
    {
        // Get new value, but always be different for better visual effect.
        if (flipCoin)
        {
            cull1Value.text = GetRandomValue().ToString();
            while(cull1Value.text == cull2Value.text)
                cull1Value.text = GetRandomValue().ToString();
        }
        else
        {
            cull2Value.text = GetRandomValue().ToString();
            while (cull1Value.text == cull2Value.text)
                cull2Value.text = GetRandomValue().ToString();
        }

        flipCoin = !flipCoin;   
    }
    
    public float GetRandomValue()
    {
        return values[Random.Range(0, values.Count - 1)];
    }

    public void SetSpinValue()
    {
        cull1Value.text = GetRandomValue().ToString();
        //cull2Value.text = "6000";
    }
}
