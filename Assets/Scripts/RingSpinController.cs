using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingSpinController : MonoBehaviour
{
    private bool    isAccelarating          = false;
    private float   currentRotationSpeed    = 0.0f;

    public Vector3  rotationAxis        = new Vector3(0f, 1f, 0f);
    public float    acceleration        = 0.1f;
    public float    maxRotationSpeed    = 2f;

    public float    decelerationRate = 1f;

    [SerializeField]
    private Transform   ringTransform;

    // Smooth accelaration
    public float    smoothTime  = 1f;
    private bool    isDecelarating = false;


    void FixedUpdate()
    {
        if (isAccelarating)
        {
            Accelarate();
            Rotate();
        }
    }

    public void StartRotating()
    {
        isAccelarating = true;
    }
    public void StopRotating()
    {
        isAccelarating = false;
        Decelarate();
    }

    private void Accelarate()
    {
        currentRotationSpeed += acceleration * Time.deltaTime;
        currentRotationSpeed = Mathf.Clamp(currentRotationSpeed, -maxRotationSpeed, maxRotationSpeed);
    }
    private void Decelarate()
    {
        if (ringTransform.localRotation != Quaternion.identity)
            StartCoroutine(DecelarateRoutine());
    }
    private IEnumerator DecelarateRoutine()
    {
        float distance = 1f;
        float startRotationSpeed = currentRotationSpeed;
        while (distance >= 0.01f)
        {
            //Find positive distance from 360.
            distance = (360f - ringTransform.localEulerAngles.y) / 360f;
            distance = Mathf.Clamp(distance, 0.001f, 1f);

            currentRotationSpeed = startRotationSpeed * (1 - Mathf.Pow(2.71828f, -decelerationRate * distance));
            Rotate();

            yield return null;
        }

        currentRotationSpeed = 0f;
        ringTransform.localEulerAngles = Vector3.zero;
    }

    private void Rotate()
    {
        ringTransform.Rotate(
            currentRotationSpeed * rotationAxis.x,
            currentRotationSpeed * rotationAxis.y,
            currentRotationSpeed * rotationAxis.z,
            Space.Self
        );
    }
}
