﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReferenceAccessor : MonoBehaviour
{
    [SerializeField]
    private List<GameObject>                references          = new List<GameObject>();
    private Dictionary<string, GameObject>  cachedGameObjects   = new Dictionary<string, GameObject>();

    private void Start()
    {
        // ~~~~~~~~
        // Early cache optimization
        Get("Ring Parent");
        Get("Ring");
        Get("Blood Parent");
        Get("Blood");

        Get("LightGroup");

        Get("TitleScreen");
        // ~~~~~~~~
    }

    public GameObject   Get(string name)
    {
        if (cachedGameObjects.ContainsKey(name))
            return cachedGameObjects[name];
        else
        {
            GameObject obj = references.Find(match => match.name == name);
            if (obj == null)
                return null;

            cachedGameObjects.Add(name, obj);
            return obj;
        }
    }
    public void         Add(GameObject obj)
    {
        references.Add(obj);
    }
    public void         Remove(GameObject obj)
    {
        if (cachedGameObjects.ContainsKey(obj.name))
            cachedGameObjects.Remove(obj.name);
        references.Remove(obj);
    }

    // Util function that does not really belong here ¯\_(ツ)_/¯
    public void         EnableOnly(HashSet<string> set)
    {
        foreach (var reference in references) {
            if (reference.name.Contains("Controller"))
                continue;

            if (set.Contains(reference.name))
                reference.SetActive(true);
            else
                reference.SetActive(false);
        }
    }
}
