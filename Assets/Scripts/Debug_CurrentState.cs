using TMPro;
using UnityEngine;

public class Debug_CurrentState : MonoBehaviour
{
    public TMP_Text             text;
    public BaseStateMachine     machine;

    // Update is called once per frame
    void Update()
    {
        text.text = $"Current State: {machine.currentState.name}";
    }
}
