using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds/SoundEffect")]
public class SoundEffect : ScriptableObject
{
    public SoundEffectType  type;
    public AudioClip        clip;
    public float            defaultVolume = 1f;
    public float            defaultDelay = 0f;
}
