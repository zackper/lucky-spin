using System;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

[CreateAssetMenu(menuName = "FSM/Actions/OnHalfRotationAction")]
public class OnHalfRotationAction : FSMAction
{
    [SerializeField]
    private GameEvent   onHalfRotation;

    // References needed
    private Transform   ringTransform;

    // Variables for logic
    public float        angleInterval   = 180f;

    private float       previousAngle;
    private float       totalAngles;
    private float       lastAngleChange;

    private float       AngleDx 
    { 
        get
        {
            return (ringTransform.localEulerAngles.y - previousAngle >= 0f) ? 
                    ringTransform.localEulerAngles.y - previousAngle : ringTransform.localEulerAngles.y + (360 - previousAngle);
        } 
    }

    public override void Initialize(BaseStateMachine machine)
    {
        // Get References 
        ringTransform   = machine.GetComponent<ReferenceAccessor>().Get("Ring").transform;
    }
    public override void OnStateEnter(BaseStateMachine machine)
    {
        // Values to detect half-rotations
        previousAngle   = ringTransform.localEulerAngles.y;
        totalAngles     = 0f;
        lastAngleChange = 0f;
    }
    public override void FixedExecute(BaseStateMachine machine)
    {
        totalAngles += AngleDx;
        previousAngle = ringTransform.localEulerAngles.y;

         //Find if more than one half- rotations have been made since previous frame.
        //Debug.Log($"~~~~~~~~");
        //Debug.Log($"{ringTransform.localEulerAngles}");
        //Debug.Log($"{AngleDx}");
        //Debug.Log($"{totalAngles}");
        //Debug.Log($"{lastAngleChange}");
        while (totalAngles - lastAngleChange > angleInterval)
        {
            //Debug.Log("Half rotation done!!!!!!!");
            lastAngleChange += angleInterval;
            onHalfRotation.Raise();
        }
    }
}
