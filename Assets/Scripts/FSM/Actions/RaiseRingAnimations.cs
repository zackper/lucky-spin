using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/RaiseRingAnimations")]
public class RaiseRingAnimations : FSMAction
{
    [SerializeField]
    private GameObject BloodPrefab;

    [Header("Animation Times")]
    public float afterTitleScreenFade = 2.5f;
    public float afterLightsWait = 0.5f;
    public float afterRaiseRingWait = 1.5f;

    public override void    OnStateEnter(BaseStateMachine machine)
    {
        machine.StartCoroutine(OnStateEnterRoutine(machine));
    }

    private IEnumerator     OnStateEnterRoutine(BaseStateMachine machine)
    {
        ReferenceAccessor accessor = machine.GetComponent<ReferenceAccessor>();

        // Disable things that need to be disabled
        accessor.EnableOnly(new HashSet<string>() 
        {
            "TitleScreen",
            "Ring Parent",
            "Ring"
        });

        // Get References
        GameObject titleScreen  = accessor.Get("TitleScreen");
        GameObject lightGroup   = accessor.Get("LightGroup");
        GameObject ringParent   = accessor.Get("Ring Parent");

        Animator titleScreenAnim    = titleScreen.GetComponent<Animator>();
        Animator ringAnim           = ringParent.GetComponent<Animator>();

        // Remove Title Screen
        if(titleScreenAnim.GetBool("isOn") == false)
        {
            // If FSM begins from here, just disable title.
            titleScreen.SetActive(false);
        }
        else
        {
            titleScreenAnim.SetBool("isOn", false);
            yield return new WaitForSeconds(afterTitleScreenFade);
        }
        // Turn on the lights
        SoundManager.Instance.PlaySound(SoundEffectType.FLICK_SWITCH, 0.8f);
        lightGroup.SetActive(true);
        yield return new WaitForSeconds(afterLightsWait);
        // Start Levitating the ring
        SoundManager.Instance.PlaySound(SoundEffectType.OBJECT_DRAG);
        ringAnim.SetBool("gameInitialized", true);
    }
}
