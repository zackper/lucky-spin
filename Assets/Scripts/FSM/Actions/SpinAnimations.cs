using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/SpinAnimations")]
public class SpinAnimations : FSMAction
{
    public float cameraShakeIntensity = 0.02f;

    [SerializeField]
    private GameEvent onHalfRotation;

    private ReferenceAccessor       accessor;
    private ValueController         valueController;
    private CameraShakeController   cameraShake;
    private LightFlickerController  lightFlicker;

    public override void OnStateEnter(BaseStateMachine machine)
    {
        // Get References
        accessor        = machine.GetComponent<ReferenceAccessor>();
        valueController = accessor.Get("ValueController").GetComponent<ValueController>();
        cameraShake     = accessor.Get("CameraShakeController").GetComponent<CameraShakeController>();
        lightFlicker    = accessor.Get("LightGroup").GetComponent<LightFlickerController>();

        accessor.EnableOnly(new HashSet<string>()
        {
            "Ring Parent", "Ring",
            "Blood Parent",
            "Values Parent",
            "LightGroup"
        });

        var spinController = accessor.Get("RingSpinController").GetComponent<RingSpinController>();
        spinController.StartRotating();

        // Disable Cull 1 value (Blood has its current place)
        valueController.cull1Value.gameObject.SetActive(false);
        valueController.Reset();

        // Play earthquake sound
        SoundManager.Instance.PlaySound(SoundEffectType.EARTHQUAKE);

        // Subscribe on first turn
        onHalfRotation.RegisterListener(OnFirstHalfRotation);
    }

    private void OnFirstHalfRotation()
    {
        // Execute only once
        onHalfRotation.UnregisterListener(OnFirstHalfRotation);

        // Disable Blood
        GameObject BloodParent = accessor.Get("Blood Parent");
        BloodParent.SetActive(false);

        // Enable Cull 1 value (It now replaces the Blood)
        valueController.cull1Value.gameObject.SetActive(true);


        // Start camera shake
        cameraShake.targetIntensity = cameraShakeIntensity;

        // Start light flickering
        //lightFlicker.isFlickering = true;
    }
}
