using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/WaitForPayAnimations")]
public class WaitForPayAnimations : FSMAction
{
    public override void OnStateEnter(BaseStateMachine machine)
    {
        machine.StartCoroutine(OnStateEnterRoutine(machine));
    }
    public override void OnStateExit(BaseStateMachine machine)
    {
        ReferenceAccessor accessor = machine.GetComponent<ReferenceAccessor>();
        SoundManager.Instance.PlaySound(SoundEffectType.FLICK_SWITCH, 0.1f);
        accessor.Get("PayButton").SetActive(false);
    }

    private IEnumerator OnStateEnterRoutine(BaseStateMachine machine)
    {
        ReferenceAccessor accessor = machine.GetComponent<ReferenceAccessor>();

        // Disable things that need to be disabled
        accessor.EnableOnly(new HashSet<string>()
        {
            "Ring Parent",
            "Ring",
            "LightGroup"
        });

        SoundManager.Instance.PlaySound(SoundEffectType.FLICK_SWITCH, 0.1f);
        accessor.Get("PayButton").SetActive(true);

        yield return null;
    }
}
