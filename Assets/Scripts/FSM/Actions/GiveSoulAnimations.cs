using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/GiveSoulAnimations")]
public class GiveSoulAnimations : FSMAction
{
    public override void OnStateEnter(BaseStateMachine machine)
    {
        machine.StartCoroutine(OnStateEnterRoutine(machine));
    }

    private IEnumerator OnStateEnterRoutine(BaseStateMachine machine)
    {
        ReferenceAccessor accessor = machine.GetComponent<ReferenceAccessor>();

        // Disable things that need to be disabled
        accessor.EnableOnly(new HashSet<string>()
        {
            "Ring Parent", 
            "Ring",
            "LightGroup"
        });

        // Pain and magic effect
        SoundManager.Instance.PlaySound(SoundEffectType.MINOR_PAIN);
        yield return new WaitForSeconds(0.5f);
        SoundManager.Instance.PlaySound(SoundEffectType.DARK_MAGIC);
        yield return new WaitForSeconds(0.2f);

        // Spawn Blood
        GameObject BloodParent = accessor.Get("Blood Parent");
        BloodParent.SetActive(true);
        BloodParent.GetComponent<Animator>().SetBool("isSpawned", true);

        yield return null;
    }
}
