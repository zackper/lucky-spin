using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/TitleScreenAnimations")]
public class TitleScreenAnimations : FSMAction
{
    [Header("Animation Times")]
    public float blackScreenTime = 0.5f;

    public override void OnStateEnter(BaseStateMachine machine)
    {
        machine.StartCoroutine(OnStateEnterRoutine(machine));
    }

    private IEnumerator OnStateEnterRoutine(BaseStateMachine machine)
    {
        ReferenceAccessor accessor = machine.GetComponent<ReferenceAccessor>();

        accessor.EnableOnly(new HashSet<string>(){}); // Disable everything

        // Get References
        GameObject  titleScreen = accessor.Get("TitleScreen");
        Animator    anim        = titleScreen.GetComponent<Animator>();

        // Wait for some time
        yield return new WaitForSeconds(blackScreenTime);
        // Start title screen animation
        titleScreen.SetActive(true);
        anim.SetBool("isOn", true);

        SoundManager.Instance.PlaySound(SoundEffectType.AMBIENT, 0.5f);
    }
}
