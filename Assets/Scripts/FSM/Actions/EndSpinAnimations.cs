using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/EndSpinAnimations")]
public class EndSpinAnimations : FSMAction
{
    private ReferenceAccessor accessor;

    public override void OnStateEnter(BaseStateMachine machine)
    {
        accessor = machine.GetComponent<ReferenceAccessor>();

        var spinController      = accessor.Get("RingSpinController").GetComponent<RingSpinController>();
        var valueController     = accessor.Get("ValueController").GetComponent<ValueController>();
        var cameraShake         = accessor.Get("CameraShakeController").GetComponent<CameraShakeController>();
        var lightFlicker        = accessor.Get("LightGroup").GetComponent<LightFlickerController>();
        var particleController  = accessor.Get("RingParticleController").GetComponent<RingParticleController>();

        // Disable things that need to be disabled
        accessor.EnableOnly(new HashSet<string>()
        {
            "Ring Parent", "Ring",
            "LightGroup", "Values Parent"
        });

        //machine.StartCoroutine(ExecuteRoutine(machine));        

        spinController.StopRotating();
        valueController.SetSpinValue();

        // Stop effects
        cameraShake.targetIntensity = 0f;
        //lightFlicker.isFlickering = false;

        machine.StartCoroutine(OnStateEnterRoutine(machine));
    }

    private IEnumerator OnStateEnterRoutine(BaseStateMachine machine)
    {
        var particleController = accessor.Get("RingParticleController").GetComponent<RingParticleController>();

        yield return new WaitForSeconds(1f);
        particleController.Burst();
        SoundManager.Instance.PlaySound(SoundEffectType.DARK_MAGIC);
    }
}