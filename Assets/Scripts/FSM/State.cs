using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/State")]
public sealed class State : BaseState
{
    public List<FSMAction>  actions         = new List<FSMAction>();
    public List<Transition> transitions     = new List<Transition>();

    public override void Initialize(BaseStateMachine machine)
    {
        foreach(var action in actions)
            action.Initialize(machine);

        isInitialized = true;
    }
    public override void OnStateEnter(BaseStateMachine machine)
    {
        foreach (var action in actions)
            action.OnStateEnter(machine);
    }
    public override void OnStateExit(BaseStateMachine machine)
    {
        foreach (var action in actions)
            action.OnStateExit(machine);
    }
    public override void Execute(BaseStateMachine machine)
    {
        Debug.Assert(isInitialized, "Initialize needs to be called first.");

        foreach (var action in actions)
            if(action.isActive)
                action.Execute(machine);

        foreach (var transition in transitions)
            transition.Execute(machine);
    }
    public override void FixedExecute(BaseStateMachine machine)
    {
        if (isInitialized == false) // Fixed update is out of temp with Update.
            return;

        foreach (var action in actions)
            if(action.isActive)
                action.FixedExecute(machine);
    }
}
