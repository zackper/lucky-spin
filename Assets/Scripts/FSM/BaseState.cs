using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState : ScriptableObject
{
    [NonSerialized]
    public bool         isInitialized = false;
    public virtual void Initialize(BaseStateMachine machine) { isInitialized = true; }
    public virtual void OnStateEnter(BaseStateMachine machine) { }
    public virtual void OnStateExit(BaseStateMachine machine) { }
    public virtual void Execute(BaseStateMachine machine) { }
    public virtual void FixedExecute(BaseStateMachine machine) { }
}
