using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSMAction : ScriptableObject
{
    public bool isActive = true;

    public virtual void Initialize(BaseStateMachine machine) { }
    public virtual void OnStateEnter(BaseStateMachine machine) { }
    public virtual void OnStateExit(BaseStateMachine machine) { }
    public virtual void Execute(BaseStateMachine machine) {}
    public virtual void FixedExecute(BaseStateMachine machine) {}
}