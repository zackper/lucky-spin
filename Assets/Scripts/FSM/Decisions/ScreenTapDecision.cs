using System;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Decision/ScreenTapDecision")]
public class ScreenTapDecision : Decision
{
    [NonSerialized]
    private bool hasUserClicked = false;
    [NonSerialized]
    private bool hasSubscribed = false;

    public override bool Decide(BaseStateMachine machine)
    {
        if(hasSubscribed == false)
        {
            InputSensor sensor = machine.GetComponent<InputSensor>();
            sensor.OnUserClick.AddListener(OnUserClick);
            hasSubscribed = true;
        }

        if (hasUserClicked)
        {
            ResetSelf(machine);
            // Return true
            return true;
        }
        else
            return false;
    }


    private void ResetSelf(BaseStateMachine machine)
    {
        // Reset decision
        InputSensor sensor = machine.GetComponent<InputSensor>();
        sensor.OnUserClick.RemoveListener(OnUserClick);

        hasUserClicked = false;
        hasSubscribed = false;
    }
    private void OnUserClick()
    {
        hasUserClicked = true;
    }
}
