using System;
using UnityEngine;


[CreateAssetMenu(menuName = "FSM/Decision/TimerDecision")]
public class TimerDecision : Decision
{
    public float secondsUntilChange;

    [NonSerialized]
    private float elapsedTime = 0f;

    public override bool Decide(BaseStateMachine machine)
    {
        elapsedTime += Time.deltaTime;
        //Debug.Log($"Elasped time: {elapsedTime}");

        if (elapsedTime > secondsUntilChange)
        {
            elapsedTime = 0f; // In case the decision is used again
            return true;
        }
        else
            return false;
    }
}
