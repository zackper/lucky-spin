using System;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "FSM/Decision/ButtonClick")]
public class ButtonClickDecision : Decision
{
    public string buttonReferenceName;

    [NonSerialized]
    private bool hasUserClicked = false;
    [NonSerialized]
    private bool hasSubscribed = false;

    public override bool Decide(BaseStateMachine machine)
    {
        if (hasSubscribed == false)
        {
            var buttonObj = machine.GetComponent<ReferenceAccessor>().Get(buttonReferenceName);
            Button button = buttonObj.GetComponent<Button>();

            button.onClick.AddListener(OnUserClick);
        }

        if (hasUserClicked)
        {
            ResetSelf(machine);
            // Return true
            return true;
        }
        else
            return false;
    }


    private void ResetSelf(BaseStateMachine machine)
    {
        // Reset decision
        var buttonObj = machine.GetComponent<ReferenceAccessor>().Get(buttonReferenceName);
        Button button = buttonObj.GetComponent<Button>();
        button.onClick.RemoveListener(OnUserClick);

        hasUserClicked = false;
        hasSubscribed = false;
    }
    private void OnUserClick()
    {
        hasUserClicked = true;
    }
}
