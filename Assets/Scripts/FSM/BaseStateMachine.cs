using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStateMachine : MonoBehaviour
{
    [SerializeField]
    private BaseState                   initialState;
    public BaseState                    currentState { get; set; }

    private Dictionary<Type, Component> cachedComponents;


    private void Awake()
    {
        Application.targetFrameRate = 60;
        Screen.SetResolution(1920, 1080, false);

        cachedComponents = new Dictionary<Type, Component>();
    }
    private void Start()
    {
        ChangeState(initialState);
    }
    private void Update()
    {
        currentState.Execute(this);
    }
    private void FixedUpdate()
    {
        currentState.FixedExecute(this);
    }

    public void     ChangeState(BaseState state)
    {
        if (currentState == state || state is RemainInState)
            return;

        if(currentState != null)
            currentState.OnStateExit(this);

        if (state.isInitialized == false)
            state.Initialize(this);
        state.OnStateEnter(this);
        currentState = state;
    }
    public new T    GetComponent<T>() where T : Component
    {
        if (cachedComponents.ContainsKey(typeof(T)))
            return cachedComponents[typeof(T)] as T;

        var component = base.GetComponent<T>();
        if (component != null)
        {
            cachedComponents.Add(typeof(T), component);
        }
        return component;
    }

}
