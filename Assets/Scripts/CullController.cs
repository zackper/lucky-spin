using UnityEngine;

public class CullController : MonoBehaviour
{
    public string cullLayerPrefix = "Cull";

    [SerializeField]
    private Camera renderCamera;

    private void Awake()
    {
        Debug.Assert(renderCamera != null, "Set RenderCamera refenrece");
    }

    // Turn on the bit using an OR operation:
    public void Show(int cullId)
    {
        renderCamera.cullingMask |= 1 << LayerMask.NameToLayer(cullLayerPrefix + cullId);
    }

    // Turn off the bit using an AND operation with the complement of the shifted int:
    public void Hide(int cullId)
    {
        renderCamera.cullingMask &= ~(1 << LayerMask.NameToLayer(cullLayerPrefix + cullId));
    }
}
