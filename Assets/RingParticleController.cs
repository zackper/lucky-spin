using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class RingParticleController : MonoBehaviour
{
    public VisualEffect burstEffect;

    public void Burst()
    {
        burstEffect.Play();
    }
}
