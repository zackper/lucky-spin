using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Debug_SoundEffects : MonoBehaviour
{
    [SerializeField]
    private GameObject buttonPrefab;

    private void Start()
    {
        var soundEffects = SoundManager.Instance.debug_soundEffects;

        foreach(var effect in soundEffects)
        {
            var buttonInstance = Instantiate(buttonPrefab, transform);
            Button button = buttonInstance.GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                SoundManager.Instance.PlaySound(effect.type);
            });

            TMP_Text text = button.transform.GetChild(0).gameObject.GetComponent<TMP_Text>(); // Bad code but nahh, just to fix sound effect volumes.
            text.text = effect.name;
        }
    }
}
